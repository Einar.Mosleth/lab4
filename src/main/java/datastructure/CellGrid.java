package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	int row;
	int column;
	CellState[][] grid;
	
    public CellGrid(int rows, int columns, CellState initialState) {
    	
    	this.column = columns;
    	this.row = rows;
    	grid = new CellState[rows][columns];

    }

    @Override
    public int numRows() {
        return this.row;
    }

    @Override
    public int numColumns() {
        return this.column;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	
    	if(indexValue(row, column)) {
    		grid[row][column] = element;
    	} 
    	else {
    		throw new IndexOutOfBoundsException("I am not the index you are looking for");
    	}
    }
    
    public boolean indexValue(int row, int column) {
    	
        return row >= 0 && row < numRows() && column >= 0 && column < numColumns();
    }

    @Override
    public CellState get(int row, int column) {
    	
    	if(indexValue(row, column)) {
    		return grid[row][column];
    	} 
    	else {
    		throw new IndexOutOfBoundsException("I am not the index you are looking for");
    	}
    	
    }

    @Override
    public IGrid copy() {
        
    	IGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.DEAD);
    	
    	for(int row = 0; row < this.row; row++) {
    		for(int col = 0; col < this.column; col++) {
    			newGrid.set(row, col, get(row, col));
    		}
    	}
    	
        return newGrid;
    }
    
}
