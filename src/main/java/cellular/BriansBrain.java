package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;


    public BriansBrain(int rows, int columns) {
    	
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } 
                else if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
                else {
                    currentGeneration.set(row,col,CellState.DYING);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        
        for (int row = 0; row < nextGeneration.numRows(); row++) {
            for (int col = 0; col < nextGeneration.numColumns(); col++) {
                currentGeneration.set(row, col, getNextCell(row, col));
            }
        }

    }

    @Override
    public CellState getNextCell(int row, int col) {
    CellState currentCellstate = currentGeneration.get(row, col);
    
        int n = countNeighbors(row, col, CellState.ALIVE);
        
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        
        else if(getCellState(row,col) == CellState.DYING){
            return CellState.DEAD;
        }
        
        else if(getCellState(row, col) == CellState.DEAD){
            if (n == 2){
                return CellState.ALIVE;
            }
        }
        return currentCellstate;
    }



    private int countNeighbors(int row, int col, CellState state) {

        int neighbors = 0;

        for (int rows = row-1; rows <= row+1; rows++){
            for (int columns = col-1; columns <= col+1; columns++){
            	if (rows >= numberOfRows() || rows < 0){
                    continue;
                }
                else if(columns >= numberOfColumns() || columns < 0){
                    continue;
                }

                else if(rows == row && columns == col){
                    continue;
                }
                else if (currentGeneration.get(rows, columns) == state){
                    neighbors++;
                }
            }
        }
        return neighbors;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
